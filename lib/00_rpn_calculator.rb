class RPNCalculator
  def initialize
    @calculator = []
  end

  def value
    @calculator.last
  end

  def push(n)
    @calculator << n
  end

  def plus
    perform_op(:+)
  end

  def minus
    perform_op(:-)
  end

  def times
    perform_op(:*)
  end

  def divide
    perform_op(:/)
  end

  def tokens(str)
    ints = ("0".."9").to_a
    tokes = []
    str.split.each do |el|
      ints.include?(el) ? tokes << el.to_i : tokes << el.to_sym
    end
    tokes
  end

  def evaluate(str)
    temp = tokens(str)
    temp.each do |el|
      push(el) if el.is_a?(Integer)
      plus if el == :+
      minus if el == :-
      times if el == :*
      divide if el == :/
    end
    value
  end

  private

  def perform_op(op)
    raise "calculator is empty" if @calculator.size < 2

    latter = @calculator.pop
    first = @calculator.pop

    case op
    when :+
      @calculator << first + latter
    when :-
      @calculator << first - latter
    when :*
      @calculator << first * latter
    when :/
      @calculator << first.fdiv(latter)
    end
  end
end
